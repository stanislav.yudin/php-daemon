<?php

/**
 * Class MailSender
 */
class MailSender
{
    private $emailTo = null;
    private $subject = null;
    private $message = null;

    public function __construct( $emailTo, $subject, $message )
    {
        $this->emailTo = $emailTo;
        $this->subject = $subject;
        $this->message = $message;
    }

    public function sendmail()
    {
        $headers = "Content-type: text/html; charset= UTF-8 \r\n" .
            "From: {$this->emailTo}\r\n" .
            'X-Mailer: PHP/' . phpversion();

        mail( $this->emailTo, $this->subject, $this->message, $headers );
    }
}
