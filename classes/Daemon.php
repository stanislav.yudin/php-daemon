<?php

/**
 * Class Demon
 */
class Daemon
{
    const URL = 'https://syn.su/testwork.php';
    const MAIL = 'cmac@ya.ru';

    private $ch = null;
    private $response = null;
    private $message = null;
    private $key = null;

    public function __construct( )
    {
        $this->init();
    }

    public function __destruct()
    {
        curl_close( $this->ch );
    }

    public function Get()
    {
        $data = [ 'method' => 'get' ];
        $response = $this->SendCURL( $data );

        if ( isset( $response->response->message, $response->response->key ) ) {
            $this->message = $response->response->message;
            $this->key = $response->response->key;

            return true;
        }

        return false;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function Success()
    {
        return isset( $this->response->response ) && $this->response->response === 'Success';
    }

    public function Update( $message )
    {
        $data = [
            'method' => 'update',
            'message' => $message
        ];
        $response = $this->SendCURL( $data );

        if ( $response !== null ) {
            $this->response = $response;
            return true;
        }

        return false;
    }

    public function getErrorCode()
    {
        return ( isset( $this->response->errorCode ) ? $this->response->errorCode : null );
    }

    public function getErrorMessage()
    {
        return ( isset( $this->response->errorMessage ) ? $this->response->errorMessage : null );
    }

    private function Init()
    {
        $this->ch = curl_init( self::URL );
    }

    private function SendCURL( array $data )
    {
        curl_setopt( $this->ch, CURLOPT_POST, true );
        curl_setopt( $this->ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $this->ch, CURLOPT_POSTFIELDS, $data );

        $result = json_decode( curl_exec( $this->ch ) );
        if ( json_last_error() === JSON_ERROR_NONE ) {
            return $result;
        }

        return null;
    }
}
