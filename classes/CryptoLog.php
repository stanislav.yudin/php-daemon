<?php

/**
 * Class CryptoLog
 */
class CryptoLog
{

    public static function Encode( $string, $key )
    {
        $string = self::codeXOR( $string, $key );
        $string = base64_encode( $string );

        return $string;
    }

    public static function Decode( $string, $key )
    {
        $string = base64_decode( $string );
        $string = self::codeXOR( $string, $key );

        return $string;
    }

    private static function codeXOR( $string, $key )
    {
        for ( $i = 0; $i < strlen( $string ); $i++ ) {
            $string[ $i ] = ( $string[ $i ] ^ $key[ $i % strlen( $key ) ] );
        }

        return $string;
    }

    public static function writelog( $message )
    {
        echo date( 'd.m.Y H:i:s' ) . " {$message}\n";
    }
}
