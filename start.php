<?php

$child_pid = pcntl_fork();
if ( $child_pid ) {
    exit();
}

posix_setsid();

$baseDir = dirname( __FILE__ );
ini_set( 'error_log', $baseDir . '/error.log' );
fclose( STDIN );
fclose( STDOUT );
fclose( STDERR );
$STDIN = fopen( '/dev/null', 'r' );
$STDOUT = fopen( $baseDir . '/application.log', 'ab' );
$STDERR = fopen( $baseDir . '/daemon.log', 'ab' );

require_once __DIR__ . '/classes/Daemon.php';
require_once __DIR__ . '/classes/CryptoLog.php';
require_once __DIR__ . '/classes/MailSender.php';

$daemon = new Daemon();

if ( $daemon->Get() ) {
    $message = CryptoLog::Encode( $daemon->getMessage(), $daemon->getKey() );

    while ( true ) {
        if ( $daemon->Update( $message ) && $daemon->Success() ) {
            CryptoLog::writelog( 'Success!' );
        } else {
            CryptoLog::writelog( "Error Num.:{$daemon->getErrorCode()}, Error Message: {$daemon->getErrorMessage()}" );

            $mail = new MailSender(
                Daemon::MAIL,
                'DAEMON ERROR',
                "Error Num.:{$daemon->getErrorCode()}, Error Message: {$daemon->getErrorMessage()}."
            );
            $mail->sendmail();

            CryptoLog::writelog( 'Daemon stopped!' );
            exit();
        }

        sleep( 3600 );
    }
}
